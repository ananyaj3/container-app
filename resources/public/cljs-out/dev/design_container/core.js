// Compiled by ClojureScript 1.10.520 {}
goog.provide('design_container.core');
goog.require('cljs.core');
goog.require('goog.dom');
goog.require('rum.core');
if((typeof design_container !== 'undefined') && (typeof design_container.core !== 'undefined') && (typeof design_container.core.app_state !== 'undefined')){
} else {
design_container.core.app_state = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"text","text",-1790561697),"App Container Atom"], null));
}
design_container.core.get_app_element = (function design_container$core$get_app_element(){
return goog.dom.getElement("app");
});
design_container.core.header = rum.core.build_defc.call(null,(function (){
return React.createElement("head",null,React.createElement("title",null,"app-container"));
}),null,"header");
design_container.core.body = rum.core.build_defc.call(null,(function (){
return React.createElement("body",null,React.createElement("div",({"id": "viewport"}),React.createElement("div",({"id": "sidebar"}),React.createElement("header",null,React.createElement("a",({"href": "#"}),"Options")),React.createElement("ul",({"className": "nav"}),React.createElement("li",null,React.createElement("a",({"href": "#"}),React.createElement("i",({"className": "view-dashboard"})),"Dashboard")),React.createElement("li",null,React.createElement("a",({"href": "#"}),React.createElement("i",({"className": "view-timeline"})),"Timeline")),React.createElement("li",null,React.createElement("a",({"href": "#"}),React.createElement("i",({"className": "view-bank-and-cash"})),"Baking and Cash")),React.createElement("li",null,React.createElement("a",({"href": "#"}),React.createElement("i",({"className": "view-sales"})),"Sales")),React.createElement("li",null,React.createElement("a",({"href": "#"}),React.createElement("i",({"className": "view-purchases"})),"Purchases")),React.createElement("li",null,React.createElement("a",({"href": "#"}),React.createElement("i",({"className": "view-contacts"})),"Contacts")),React.createElement("li",null,React.createElement("a",({"href": "#"}),React.createElement("i",({"className": "view-reports"})),"Reports")),React.createElement("li",null,React.createElement("a",({"href": "#"}),React.createElement("i",({"className": "view-documents"})),"Documents")),React.createElement("li",null,React.createElement("a",({"href": "#"}),React.createElement("i",({"className": "view-chart-of-accounts"})),"Chart of Accounts")))),React.createElement("div",({"id": "content"}),React.createElement("nav",({"className": "navbar-default"}),React.createElement("div",({"className": "container-fluid"}),React.createElement("ul",({"className": "navbar-right"}),React.createElement("li",null,React.createElement("a",({"href": "#"}),"User Profile")),React.createElement("li",null,React.createElement("a",({"href": "#"}),"Notifications")),React.createElement("li",null,React.createElement("a",({"href": "#"}),"Help"))))),React.createElement("div",({"className": "content-class"}),React.createElement("h1",null,"Basic Container App"),React.createElement("p",null,"Keep Content Here")))));
}),null,"body");
design_container.core.content = rum.core.build_defc.call(null,(function (){
return React.createElement("div",null,sablono.interpreter.interpret.call(null,design_container.core.header.call(null)),sablono.interpreter.interpret.call(null,design_container.core.body.call(null)));
}),null,"content");
design_container.core.mount = (function design_container$core$mount(el){
return rum.core.mount.call(null,design_container.core.content.call(null),el);
});
design_container.core.mount_app_element = (function design_container$core$mount_app_element(){
var temp__5457__auto__ = design_container.core.get_app_element.call(null);
if(cljs.core.truth_(temp__5457__auto__)){
var el = temp__5457__auto__;
return design_container.core.mount.call(null,el);
} else {
return null;
}
});
design_container.core.mount_app_element.call(null);
design_container.core.on_reload = (function design_container$core$on_reload(){
return design_container.core.mount_app_element.call(null);
});

//# sourceMappingURL=core.js.map
