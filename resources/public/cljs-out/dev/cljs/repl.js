// Compiled by ClojureScript 1.10.520 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
goog.require('goog.string');
goog.require('goog.string.format');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__21495){
var map__21496 = p__21495;
var map__21496__$1 = (((((!((map__21496 == null))))?(((((map__21496.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__21496.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21496):map__21496);
var m = map__21496__$1;
var n = cljs.core.get.call(null,map__21496__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.call(null,map__21496__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,(function (){var or__4131__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return [(function (){var temp__5457__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5457__auto__)){
var ns = temp__5457__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})());

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__21498_21530 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__21499_21531 = null;
var count__21500_21532 = (0);
var i__21501_21533 = (0);
while(true){
if((i__21501_21533 < count__21500_21532)){
var f_21534 = cljs.core._nth.call(null,chunk__21499_21531,i__21501_21533);
cljs.core.println.call(null,"  ",f_21534);


var G__21535 = seq__21498_21530;
var G__21536 = chunk__21499_21531;
var G__21537 = count__21500_21532;
var G__21538 = (i__21501_21533 + (1));
seq__21498_21530 = G__21535;
chunk__21499_21531 = G__21536;
count__21500_21532 = G__21537;
i__21501_21533 = G__21538;
continue;
} else {
var temp__5457__auto___21539 = cljs.core.seq.call(null,seq__21498_21530);
if(temp__5457__auto___21539){
var seq__21498_21540__$1 = temp__5457__auto___21539;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__21498_21540__$1)){
var c__4550__auto___21541 = cljs.core.chunk_first.call(null,seq__21498_21540__$1);
var G__21542 = cljs.core.chunk_rest.call(null,seq__21498_21540__$1);
var G__21543 = c__4550__auto___21541;
var G__21544 = cljs.core.count.call(null,c__4550__auto___21541);
var G__21545 = (0);
seq__21498_21530 = G__21542;
chunk__21499_21531 = G__21543;
count__21500_21532 = G__21544;
i__21501_21533 = G__21545;
continue;
} else {
var f_21546 = cljs.core.first.call(null,seq__21498_21540__$1);
cljs.core.println.call(null,"  ",f_21546);


var G__21547 = cljs.core.next.call(null,seq__21498_21540__$1);
var G__21548 = null;
var G__21549 = (0);
var G__21550 = (0);
seq__21498_21530 = G__21547;
chunk__21499_21531 = G__21548;
count__21500_21532 = G__21549;
i__21501_21533 = G__21550;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_21551 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__4131__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_21551);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_21551)))?cljs.core.second.call(null,arglists_21551):arglists_21551));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Spec");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__21502_21552 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__21503_21553 = null;
var count__21504_21554 = (0);
var i__21505_21555 = (0);
while(true){
if((i__21505_21555 < count__21504_21554)){
var vec__21516_21556 = cljs.core._nth.call(null,chunk__21503_21553,i__21505_21555);
var name_21557 = cljs.core.nth.call(null,vec__21516_21556,(0),null);
var map__21519_21558 = cljs.core.nth.call(null,vec__21516_21556,(1),null);
var map__21519_21559__$1 = (((((!((map__21519_21558 == null))))?(((((map__21519_21558.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__21519_21558.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21519_21558):map__21519_21558);
var doc_21560 = cljs.core.get.call(null,map__21519_21559__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_21561 = cljs.core.get.call(null,map__21519_21559__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_21557);

cljs.core.println.call(null," ",arglists_21561);

if(cljs.core.truth_(doc_21560)){
cljs.core.println.call(null," ",doc_21560);
} else {
}


var G__21562 = seq__21502_21552;
var G__21563 = chunk__21503_21553;
var G__21564 = count__21504_21554;
var G__21565 = (i__21505_21555 + (1));
seq__21502_21552 = G__21562;
chunk__21503_21553 = G__21563;
count__21504_21554 = G__21564;
i__21505_21555 = G__21565;
continue;
} else {
var temp__5457__auto___21566 = cljs.core.seq.call(null,seq__21502_21552);
if(temp__5457__auto___21566){
var seq__21502_21567__$1 = temp__5457__auto___21566;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__21502_21567__$1)){
var c__4550__auto___21568 = cljs.core.chunk_first.call(null,seq__21502_21567__$1);
var G__21569 = cljs.core.chunk_rest.call(null,seq__21502_21567__$1);
var G__21570 = c__4550__auto___21568;
var G__21571 = cljs.core.count.call(null,c__4550__auto___21568);
var G__21572 = (0);
seq__21502_21552 = G__21569;
chunk__21503_21553 = G__21570;
count__21504_21554 = G__21571;
i__21505_21555 = G__21572;
continue;
} else {
var vec__21521_21573 = cljs.core.first.call(null,seq__21502_21567__$1);
var name_21574 = cljs.core.nth.call(null,vec__21521_21573,(0),null);
var map__21524_21575 = cljs.core.nth.call(null,vec__21521_21573,(1),null);
var map__21524_21576__$1 = (((((!((map__21524_21575 == null))))?(((((map__21524_21575.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__21524_21575.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21524_21575):map__21524_21575);
var doc_21577 = cljs.core.get.call(null,map__21524_21576__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_21578 = cljs.core.get.call(null,map__21524_21576__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_21574);

cljs.core.println.call(null," ",arglists_21578);

if(cljs.core.truth_(doc_21577)){
cljs.core.println.call(null," ",doc_21577);
} else {
}


var G__21579 = cljs.core.next.call(null,seq__21502_21567__$1);
var G__21580 = null;
var G__21581 = (0);
var G__21582 = (0);
seq__21502_21552 = G__21579;
chunk__21503_21553 = G__21580;
count__21504_21554 = G__21581;
i__21505_21555 = G__21582;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5457__auto__ = cljs.spec.alpha.get_spec.call(null,cljs.core.symbol.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name.call(null,n)),cljs.core.name.call(null,nm)));
if(cljs.core.truth_(temp__5457__auto__)){
var fnspec = temp__5457__auto__;
cljs.core.print.call(null,"Spec");

var seq__21526 = cljs.core.seq.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__21527 = null;
var count__21528 = (0);
var i__21529 = (0);
while(true){
if((i__21529 < count__21528)){
var role = cljs.core._nth.call(null,chunk__21527,i__21529);
var temp__5457__auto___21583__$1 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5457__auto___21583__$1)){
var spec_21584 = temp__5457__auto___21583__$1;
cljs.core.print.call(null,["\n ",cljs.core.name.call(null,role),":"].join(''),cljs.spec.alpha.describe.call(null,spec_21584));
} else {
}


var G__21585 = seq__21526;
var G__21586 = chunk__21527;
var G__21587 = count__21528;
var G__21588 = (i__21529 + (1));
seq__21526 = G__21585;
chunk__21527 = G__21586;
count__21528 = G__21587;
i__21529 = G__21588;
continue;
} else {
var temp__5457__auto____$1 = cljs.core.seq.call(null,seq__21526);
if(temp__5457__auto____$1){
var seq__21526__$1 = temp__5457__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__21526__$1)){
var c__4550__auto__ = cljs.core.chunk_first.call(null,seq__21526__$1);
var G__21589 = cljs.core.chunk_rest.call(null,seq__21526__$1);
var G__21590 = c__4550__auto__;
var G__21591 = cljs.core.count.call(null,c__4550__auto__);
var G__21592 = (0);
seq__21526 = G__21589;
chunk__21527 = G__21590;
count__21528 = G__21591;
i__21529 = G__21592;
continue;
} else {
var role = cljs.core.first.call(null,seq__21526__$1);
var temp__5457__auto___21593__$2 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5457__auto___21593__$2)){
var spec_21594 = temp__5457__auto___21593__$2;
cljs.core.print.call(null,["\n ",cljs.core.name.call(null,role),":"].join(''),cljs.spec.alpha.describe.call(null,spec_21594));
} else {
}


var G__21595 = cljs.core.next.call(null,seq__21526__$1);
var G__21596 = null;
var G__21597 = (0);
var G__21598 = (0);
seq__21526 = G__21595;
chunk__21527 = G__21596;
count__21528 = G__21597;
i__21529 = G__21598;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol(null,"ExceptionInfo","ExceptionInfo",294935087,null):(((t instanceof EvalError))?new cljs.core.Symbol("js","EvalError","js/EvalError",1793498501,null):(((t instanceof RangeError))?new cljs.core.Symbol("js","RangeError","js/RangeError",1703848089,null):(((t instanceof ReferenceError))?new cljs.core.Symbol("js","ReferenceError","js/ReferenceError",-198403224,null):(((t instanceof SyntaxError))?new cljs.core.Symbol("js","SyntaxError","js/SyntaxError",-1527651665,null):(((t instanceof URIError))?new cljs.core.Symbol("js","URIError","js/URIError",505061350,null):(((t instanceof Error))?new cljs.core.Symbol("js","Error","js/Error",-1692659266,null):null
)))))))], null),(function (){var temp__5457__auto__ = cljs.core.ex_message.call(null,t);
if(cljs.core.truth_(temp__5457__auto__)){
var msg = temp__5457__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5457__auto__ = cljs.core.ex_data.call(null,t);
if(cljs.core.truth_(temp__5457__auto__)){
var ed = temp__5457__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})());
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__21599 = cljs.core.conj.call(null,via,t);
var G__21600 = cljs.core.ex_cause.call(null,t);
via = G__21599;
t = G__21600;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek.call(null,via);
return cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec.call(null,cljs.core.map.call(null,base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5457__auto__ = cljs.core.ex_message.call(null,root);
if(cljs.core.truth_(temp__5457__auto__)){
var root_msg = temp__5457__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5457__auto__ = cljs.core.ex_data.call(null,root);
if(cljs.core.truth_(temp__5457__auto__)){
var data = temp__5457__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5457__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data.call(null,o));
if(cljs.core.truth_(temp__5457__auto__)){
var phase = temp__5457__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})());
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__21603 = datafied_throwable;
var map__21603__$1 = (((((!((map__21603 == null))))?(((((map__21603.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__21603.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21603):map__21603);
var via = cljs.core.get.call(null,map__21603__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.call(null,map__21603__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.call(null,map__21603__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__21604 = cljs.core.last.call(null,via);
var map__21604__$1 = (((((!((map__21604 == null))))?(((((map__21604.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__21604.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21604):map__21604);
var type = cljs.core.get.call(null,map__21604__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.call(null,map__21604__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.call(null,map__21604__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__21605 = data;
var map__21605__$1 = (((((!((map__21605 == null))))?(((((map__21605.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__21605.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21605):map__21605);
var problems = cljs.core.get.call(null,map__21605__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.call(null,map__21605__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.call(null,map__21605__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__21606 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first.call(null,via));
var map__21606__$1 = (((((!((map__21606 == null))))?(((((map__21606.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__21606.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21606):map__21606);
var top_data = map__21606__$1;
var source = cljs.core.get.call(null,map__21606__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.call(null,(function (){var G__21611 = phase;
var G__21611__$1 = (((G__21611 instanceof cljs.core.Keyword))?G__21611.fqn:null);
switch (G__21611__$1) {
case "read-source":
var map__21612 = data;
var map__21612__$1 = (((((!((map__21612 == null))))?(((((map__21612.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__21612.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21612):map__21612);
var line = cljs.core.get.call(null,map__21612__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.call(null,map__21612__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__21614 = cljs.core.merge.call(null,new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second.call(null,via)),top_data);
var G__21614__$1 = (cljs.core.truth_(source)?cljs.core.assoc.call(null,G__21614,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__21614);
var G__21614__$2 = (cljs.core.truth_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null).call(null,source))?cljs.core.dissoc.call(null,G__21614__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__21614__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.call(null,G__21614__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__21614__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__21615 = top_data;
var G__21615__$1 = (cljs.core.truth_(source)?cljs.core.assoc.call(null,G__21615,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__21615);
var G__21615__$2 = (cljs.core.truth_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null).call(null,source))?cljs.core.dissoc.call(null,G__21615__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__21615__$1);
var G__21615__$3 = (cljs.core.truth_(type)?cljs.core.assoc.call(null,G__21615__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__21615__$2);
var G__21615__$4 = (cljs.core.truth_(message)?cljs.core.assoc.call(null,G__21615__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__21615__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.call(null,G__21615__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__21615__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__21616 = cljs.core.first.call(null,trace);
var source__$1 = cljs.core.nth.call(null,vec__21616,(0),null);
var method = cljs.core.nth.call(null,vec__21616,(1),null);
var file = cljs.core.nth.call(null,vec__21616,(2),null);
var line = cljs.core.nth.call(null,vec__21616,(3),null);
var G__21619 = top_data;
var G__21619__$1 = (cljs.core.truth_(line)?cljs.core.assoc.call(null,G__21619,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__21619);
var G__21619__$2 = (cljs.core.truth_(file)?cljs.core.assoc.call(null,G__21619__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__21619__$1);
var G__21619__$3 = (cljs.core.truth_((function (){var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
})())?cljs.core.assoc.call(null,G__21619__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__21619__$2);
var G__21619__$4 = (cljs.core.truth_(type)?cljs.core.assoc.call(null,G__21619__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__21619__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.call(null,G__21619__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__21619__$4;
}

break;
case "execution":
var vec__21620 = cljs.core.first.call(null,trace);
var source__$1 = cljs.core.nth.call(null,vec__21620,(0),null);
var method = cljs.core.nth.call(null,vec__21620,(1),null);
var file = cljs.core.nth.call(null,vec__21620,(2),null);
var line = cljs.core.nth.call(null,vec__21620,(3),null);
var file__$1 = cljs.core.first.call(null,cljs.core.remove.call(null,((function (vec__21620,source__$1,method,file,line,G__21611,G__21611__$1,map__21603,map__21603__$1,via,trace,phase,map__21604,map__21604__$1,type,message,data,map__21605,map__21605__$1,problems,fn,caller,map__21606,map__21606__$1,top_data,source){
return (function (p1__21602_SHARP_){
var or__4131__auto__ = (p1__21602_SHARP_ == null);
if(or__4131__auto__){
return or__4131__auto__;
} else {
return new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null).call(null,p1__21602_SHARP_);
}
});})(vec__21620,source__$1,method,file,line,G__21611,G__21611__$1,map__21603,map__21603__$1,via,trace,phase,map__21604,map__21604__$1,type,message,data,map__21605,map__21605__$1,problems,fn,caller,map__21606,map__21606__$1,top_data,source))
,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__4131__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return line;
}
})();
var G__21623 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__21623__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.call(null,G__21623,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__21623);
var G__21623__$2 = (cljs.core.truth_(message)?cljs.core.assoc.call(null,G__21623__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__21623__$1);
var G__21623__$3 = (cljs.core.truth_((function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
}
})())?cljs.core.assoc.call(null,G__21623__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__21623__$2);
var G__21623__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.call(null,G__21623__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__21623__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.call(null,G__21623__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__21623__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__21611__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__21627){
var map__21628 = p__21627;
var map__21628__$1 = (((((!((map__21628 == null))))?(((((map__21628.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__21628.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21628):map__21628);
var triage_data = map__21628__$1;
var phase = cljs.core.get.call(null,map__21628__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.call(null,map__21628__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.call(null,map__21628__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.call(null,map__21628__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.call(null,map__21628__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.call(null,map__21628__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.call(null,map__21628__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.call(null,map__21628__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = source;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = line;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name.call(null,(function (){var or__4131__auto__ = class$;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__21630 = phase;
var G__21630__$1 = (((G__21630 instanceof cljs.core.Keyword))?G__21630.fqn:null);
switch (G__21630__$1) {
case "read-source":
return format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause);

break;
case "macro-syntax-check":
return format.call(null,"Syntax error macroexpanding %sat (%s).\n%s",(cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):""),loc,(cljs.core.truth_(spec)?(function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__21631_21640 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__21632_21641 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__21633_21642 = true;
var _STAR_print_fn_STAR__temp_val__21634_21643 = ((function (_STAR_print_newline_STAR__orig_val__21631_21640,_STAR_print_fn_STAR__orig_val__21632_21641,_STAR_print_newline_STAR__temp_val__21633_21642,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__21631_21640,_STAR_print_fn_STAR__orig_val__21632_21641,_STAR_print_newline_STAR__temp_val__21633_21642,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__21633_21642;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__21634_21643;

try{cljs.spec.alpha.explain_out.call(null,cljs.core.update.call(null,spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__21631_21640,_STAR_print_fn_STAR__orig_val__21632_21641,_STAR_print_newline_STAR__temp_val__21633_21642,_STAR_print_fn_STAR__temp_val__21634_21643,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.call(null,((function (_STAR_print_newline_STAR__orig_val__21631_21640,_STAR_print_fn_STAR__orig_val__21632_21641,_STAR_print_newline_STAR__temp_val__21633_21642,_STAR_print_fn_STAR__temp_val__21634_21643,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__21625_SHARP_){
return cljs.core.dissoc.call(null,p1__21625_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__21631_21640,_STAR_print_fn_STAR__orig_val__21632_21641,_STAR_print_newline_STAR__temp_val__21633_21642,_STAR_print_fn_STAR__temp_val__21634_21643,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__21631_21640,_STAR_print_fn_STAR__orig_val__21632_21641,_STAR_print_newline_STAR__temp_val__21633_21642,_STAR_print_fn_STAR__temp_val__21634_21643,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__21632_21641;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__21631_21640;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})():format.call(null,"%s\n",cause)));

break;
case "macroexpansion":
return format.call(null,"Unexpected error%s macroexpanding %sat (%s).\n%s\n",cause_type,(cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):""),loc,cause);

break;
case "compile-syntax-check":
return format.call(null,"Syntax error%s compiling %sat (%s).\n%s\n",cause_type,(cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):""),loc,cause);

break;
case "compilation":
return format.call(null,"Unexpected error%s compiling %sat (%s).\n%s\n",cause_type,(cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):""),loc,cause);

break;
case "read-eval-result":
return format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause);

break;
case "print-eval-result":
return format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause);

break;
case "execution":
if(cljs.core.truth_(spec)){
return format.call(null,"Execution error - invalid arguments to %s at (%s).\n%s",symbol,loc,(function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__21635_21644 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__21636_21645 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__21637_21646 = true;
var _STAR_print_fn_STAR__temp_val__21638_21647 = ((function (_STAR_print_newline_STAR__orig_val__21635_21644,_STAR_print_fn_STAR__orig_val__21636_21645,_STAR_print_newline_STAR__temp_val__21637_21646,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__21635_21644,_STAR_print_fn_STAR__orig_val__21636_21645,_STAR_print_newline_STAR__temp_val__21637_21646,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__21637_21646;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__21638_21647;

try{cljs.spec.alpha.explain_out.call(null,cljs.core.update.call(null,spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__21635_21644,_STAR_print_fn_STAR__orig_val__21636_21645,_STAR_print_newline_STAR__temp_val__21637_21646,_STAR_print_fn_STAR__temp_val__21638_21647,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.call(null,((function (_STAR_print_newline_STAR__orig_val__21635_21644,_STAR_print_fn_STAR__orig_val__21636_21645,_STAR_print_newline_STAR__temp_val__21637_21646,_STAR_print_fn_STAR__temp_val__21638_21647,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__21626_SHARP_){
return cljs.core.dissoc.call(null,p1__21626_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__21635_21644,_STAR_print_fn_STAR__orig_val__21636_21645,_STAR_print_newline_STAR__temp_val__21637_21646,_STAR_print_fn_STAR__temp_val__21638_21647,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__21635_21644,_STAR_print_fn_STAR__orig_val__21636_21645,_STAR_print_newline_STAR__temp_val__21637_21646,_STAR_print_fn_STAR__temp_val__21638_21647,sb__4661__auto__,G__21630,G__21630__$1,loc,class_name,simple_class,cause_type,format,map__21628,map__21628__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__21636_21645;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__21635_21644;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})());
} else {
return format.call(null,"Execution error%s at %s(%s).\n%s\n",cause_type,(cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):""),loc,cause);
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__21630__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str.call(null,cljs.repl.ex_triage.call(null,cljs.repl.Error__GT_map.call(null,error)));
});

//# sourceMappingURL=repl.js.map
