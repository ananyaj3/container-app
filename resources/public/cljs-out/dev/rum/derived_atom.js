// Compiled by ClojureScript 1.10.520 {}
goog.provide('rum.derived_atom');
goog.require('cljs.core');
rum.derived_atom.derived_atom = (function rum$derived_atom$derived_atom(var_args){
var G__10428 = arguments.length;
switch (G__10428) {
case 3:
return rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$3 = (function (refs,key,f){
return rum.derived_atom.derived_atom.call(null,refs,key,f,cljs.core.PersistentArrayMap.EMPTY);
});

rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$4 = (function (refs,key,f,opts){
var map__10429 = opts;
var map__10429__$1 = (((((!((map__10429 == null))))?(((((map__10429.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__10429.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__10429):map__10429);
var ref = cljs.core.get.call(null,map__10429__$1,new cljs.core.Keyword(null,"ref","ref",1289896967));
var check_equals_QMARK_ = cljs.core.get.call(null,map__10429__$1,new cljs.core.Keyword(null,"check-equals?","check-equals?",-2005755315),true);
var recalc = (function (){var G__10431 = cljs.core.count.call(null,refs);
switch (G__10431) {
case (1):
var vec__10432 = refs;
var a = cljs.core.nth.call(null,vec__10432,(0),null);
return ((function (vec__10432,a,G__10431,map__10429,map__10429__$1,ref,check_equals_QMARK_){
return (function (){
return f.call(null,cljs.core.deref.call(null,a));
});
;})(vec__10432,a,G__10431,map__10429,map__10429__$1,ref,check_equals_QMARK_))

break;
case (2):
var vec__10435 = refs;
var a = cljs.core.nth.call(null,vec__10435,(0),null);
var b = cljs.core.nth.call(null,vec__10435,(1),null);
return ((function (vec__10435,a,b,G__10431,map__10429,map__10429__$1,ref,check_equals_QMARK_){
return (function (){
return f.call(null,cljs.core.deref.call(null,a),cljs.core.deref.call(null,b));
});
;})(vec__10435,a,b,G__10431,map__10429,map__10429__$1,ref,check_equals_QMARK_))

break;
case (3):
var vec__10438 = refs;
var a = cljs.core.nth.call(null,vec__10438,(0),null);
var b = cljs.core.nth.call(null,vec__10438,(1),null);
var c = cljs.core.nth.call(null,vec__10438,(2),null);
return ((function (vec__10438,a,b,c,G__10431,map__10429,map__10429__$1,ref,check_equals_QMARK_){
return (function (){
return f.call(null,cljs.core.deref.call(null,a),cljs.core.deref.call(null,b),cljs.core.deref.call(null,c));
});
;})(vec__10438,a,b,c,G__10431,map__10429,map__10429__$1,ref,check_equals_QMARK_))

break;
default:
return ((function (G__10431,map__10429,map__10429__$1,ref,check_equals_QMARK_){
return (function (){
return cljs.core.apply.call(null,f,cljs.core.map.call(null,cljs.core.deref,refs));
});
;})(G__10431,map__10429,map__10429__$1,ref,check_equals_QMARK_))

}
})();
var sink = (cljs.core.truth_(ref)?(function (){var G__10441 = ref;
cljs.core.reset_BANG_.call(null,G__10441,recalc.call(null));

return G__10441;
})():cljs.core.atom.call(null,recalc.call(null)));
var watch = (cljs.core.truth_(check_equals_QMARK_)?((function (map__10429,map__10429__$1,ref,check_equals_QMARK_,recalc,sink){
return (function (_,___$1,___$2,___$3){
var new_val = recalc.call(null);
if(cljs.core.not_EQ_.call(null,cljs.core.deref.call(null,sink),new_val)){
return cljs.core.reset_BANG_.call(null,sink,new_val);
} else {
return null;
}
});})(map__10429,map__10429__$1,ref,check_equals_QMARK_,recalc,sink))
:((function (map__10429,map__10429__$1,ref,check_equals_QMARK_,recalc,sink){
return (function (_,___$1,___$2,___$3){
return cljs.core.reset_BANG_.call(null,sink,recalc.call(null));
});})(map__10429,map__10429__$1,ref,check_equals_QMARK_,recalc,sink))
);
var seq__10442_10448 = cljs.core.seq.call(null,refs);
var chunk__10443_10449 = null;
var count__10444_10450 = (0);
var i__10445_10451 = (0);
while(true){
if((i__10445_10451 < count__10444_10450)){
var ref_10452__$1 = cljs.core._nth.call(null,chunk__10443_10449,i__10445_10451);
cljs.core.add_watch.call(null,ref_10452__$1,key,watch);


var G__10453 = seq__10442_10448;
var G__10454 = chunk__10443_10449;
var G__10455 = count__10444_10450;
var G__10456 = (i__10445_10451 + (1));
seq__10442_10448 = G__10453;
chunk__10443_10449 = G__10454;
count__10444_10450 = G__10455;
i__10445_10451 = G__10456;
continue;
} else {
var temp__5457__auto___10457 = cljs.core.seq.call(null,seq__10442_10448);
if(temp__5457__auto___10457){
var seq__10442_10458__$1 = temp__5457__auto___10457;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__10442_10458__$1)){
var c__4550__auto___10459 = cljs.core.chunk_first.call(null,seq__10442_10458__$1);
var G__10460 = cljs.core.chunk_rest.call(null,seq__10442_10458__$1);
var G__10461 = c__4550__auto___10459;
var G__10462 = cljs.core.count.call(null,c__4550__auto___10459);
var G__10463 = (0);
seq__10442_10448 = G__10460;
chunk__10443_10449 = G__10461;
count__10444_10450 = G__10462;
i__10445_10451 = G__10463;
continue;
} else {
var ref_10464__$1 = cljs.core.first.call(null,seq__10442_10458__$1);
cljs.core.add_watch.call(null,ref_10464__$1,key,watch);


var G__10465 = cljs.core.next.call(null,seq__10442_10458__$1);
var G__10466 = null;
var G__10467 = (0);
var G__10468 = (0);
seq__10442_10448 = G__10465;
chunk__10443_10449 = G__10466;
count__10444_10450 = G__10467;
i__10445_10451 = G__10468;
continue;
}
} else {
}
}
break;
}

return sink;
});

rum.derived_atom.derived_atom.cljs$lang$maxFixedArity = 4;


//# sourceMappingURL=derived_atom.js.map
