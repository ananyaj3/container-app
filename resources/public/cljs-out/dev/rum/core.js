// Compiled by ClojureScript 1.10.520 {}
goog.provide('rum.core');
goog.require('cljs.core');
goog.require('cljsjs.react');
goog.require('cljsjs.react.dom');
goog.require('goog.object');
goog.require('sablono.core');
goog.require('rum.cursor');
goog.require('rum.util');
goog.require('rum.derived_atom');
/**
 * Given React component, returns Rum state associated with it
 */
rum.core.state = (function rum$core$state(comp){
return goog.object.get(comp.state,":rum/state");
});
rum.core.extend_BANG_ = (function rum$core$extend_BANG_(obj,props){
var seq__12746 = cljs.core.seq.call(null,props);
var chunk__12748 = null;
var count__12749 = (0);
var i__12750 = (0);
while(true){
if((i__12750 < count__12749)){
var vec__12758 = cljs.core._nth.call(null,chunk__12748,i__12750);
var k = cljs.core.nth.call(null,vec__12758,(0),null);
var v = cljs.core.nth.call(null,vec__12758,(1),null);
if((!((v == null)))){
goog.object.set(obj,cljs.core.name.call(null,k),cljs.core.clj__GT_js.call(null,v));


var G__12764 = seq__12746;
var G__12765 = chunk__12748;
var G__12766 = count__12749;
var G__12767 = (i__12750 + (1));
seq__12746 = G__12764;
chunk__12748 = G__12765;
count__12749 = G__12766;
i__12750 = G__12767;
continue;
} else {
var G__12768 = seq__12746;
var G__12769 = chunk__12748;
var G__12770 = count__12749;
var G__12771 = (i__12750 + (1));
seq__12746 = G__12768;
chunk__12748 = G__12769;
count__12749 = G__12770;
i__12750 = G__12771;
continue;
}
} else {
var temp__5457__auto__ = cljs.core.seq.call(null,seq__12746);
if(temp__5457__auto__){
var seq__12746__$1 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12746__$1)){
var c__4550__auto__ = cljs.core.chunk_first.call(null,seq__12746__$1);
var G__12772 = cljs.core.chunk_rest.call(null,seq__12746__$1);
var G__12773 = c__4550__auto__;
var G__12774 = cljs.core.count.call(null,c__4550__auto__);
var G__12775 = (0);
seq__12746 = G__12772;
chunk__12748 = G__12773;
count__12749 = G__12774;
i__12750 = G__12775;
continue;
} else {
var vec__12761 = cljs.core.first.call(null,seq__12746__$1);
var k = cljs.core.nth.call(null,vec__12761,(0),null);
var v = cljs.core.nth.call(null,vec__12761,(1),null);
if((!((v == null)))){
goog.object.set(obj,cljs.core.name.call(null,k),cljs.core.clj__GT_js.call(null,v));


var G__12776 = cljs.core.next.call(null,seq__12746__$1);
var G__12777 = null;
var G__12778 = (0);
var G__12779 = (0);
seq__12746 = G__12776;
chunk__12748 = G__12777;
count__12749 = G__12778;
i__12750 = G__12779;
continue;
} else {
var G__12780 = cljs.core.next.call(null,seq__12746__$1);
var G__12781 = null;
var G__12782 = (0);
var G__12783 = (0);
seq__12746 = G__12780;
chunk__12748 = G__12781;
count__12749 = G__12782;
i__12750 = G__12783;
continue;
}
}
} else {
return null;
}
}
break;
}
});
rum.core.build_class = (function rum$core$build_class(render,mixins,display_name){
var init = rum.util.collect.call(null,new cljs.core.Keyword(null,"init","init",-1875481434),mixins);
var will_mount = rum.util.collect_STAR_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"will-mount","will-mount",-434633071),new cljs.core.Keyword(null,"before-render","before-render",71256781)], null),mixins);
var render__$1 = render;
var wrap_render = rum.util.collect.call(null,new cljs.core.Keyword(null,"wrap-render","wrap-render",1782000986),mixins);
var wrapped_render = cljs.core.reduce.call(null,((function (init,will_mount,render__$1,wrap_render){
return (function (p1__12785_SHARP_,p2__12784_SHARP_){
return p2__12784_SHARP_.call(null,p1__12785_SHARP_);
});})(init,will_mount,render__$1,wrap_render))
,render__$1,wrap_render);
var did_mount = rum.util.collect_STAR_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"did-mount","did-mount",918232960),new cljs.core.Keyword(null,"after-render","after-render",1997533433)], null),mixins);
var did_remount = rum.util.collect.call(null,new cljs.core.Keyword(null,"did-remount","did-remount",1362550500),mixins);
var should_update = rum.util.collect.call(null,new cljs.core.Keyword(null,"should-update","should-update",-1292781795),mixins);
var will_update = rum.util.collect_STAR_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"will-update","will-update",328062998),new cljs.core.Keyword(null,"before-render","before-render",71256781)], null),mixins);
var did_update = rum.util.collect_STAR_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"did-update","did-update",-2143702256),new cljs.core.Keyword(null,"after-render","after-render",1997533433)], null),mixins);
var did_catch = rum.util.collect.call(null,new cljs.core.Keyword(null,"did-catch","did-catch",2139522313),mixins);
var will_unmount = rum.util.collect.call(null,new cljs.core.Keyword(null,"will-unmount","will-unmount",-808051550),mixins);
var child_context = rum.util.collect.call(null,new cljs.core.Keyword(null,"child-context","child-context",-1375270295),mixins);
var class_props = cljs.core.reduce.call(null,cljs.core.merge,rum.util.collect.call(null,new cljs.core.Keyword(null,"class-properties","class-properties",1351279702),mixins));
var static_props = cljs.core.reduce.call(null,cljs.core.merge,rum.util.collect.call(null,new cljs.core.Keyword(null,"static-properties","static-properties",-577838503),mixins));
var ctor = ((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props){
return (function (props){
var this$ = this;
goog.object.set(this$,"state",({":rum/state": cljs.core.volatile_BANG_.call(null,rum.util.call_all.call(null,cljs.core.assoc.call(null,goog.object.get(props,":rum/initial-state"),new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248),this$),init,props))}));

return React.Component.call(this$,props);
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props))
;
var _ = goog.inherits(ctor,React.Component);
var prototype = goog.object.get(ctor,"prototype");
if(cljs.core.empty_QMARK_.call(null,will_mount)){
} else {
goog.object.set(prototype,"componentWillMount",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (){
var this$ = this;
return cljs.core._vreset_BANG_.call(null,rum.core.state.call(null,this$),rum.util.call_all.call(null,cljs.core._deref.call(null,rum.core.state.call(null,this$)),will_mount));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

if(cljs.core.empty_QMARK_.call(null,did_mount)){
} else {
goog.object.set(prototype,"componentDidMount",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (){
var this$ = this;
return cljs.core._vreset_BANG_.call(null,rum.core.state.call(null,this$),rum.util.call_all.call(null,cljs.core._deref.call(null,rum.core.state.call(null,this$)),did_mount));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

goog.object.set(prototype,"componentWillReceiveProps",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (next_props){
var this$ = this;
var old_state = cljs.core.deref.call(null,rum.core.state.call(null,this$));
var state = cljs.core.merge.call(null,old_state,goog.object.get(next_props,":rum/initial-state"));
var next_state = cljs.core.reduce.call(null,((function (old_state,state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (p1__12787_SHARP_,p2__12786_SHARP_){
return p2__12786_SHARP_.call(null,old_state,p1__12787_SHARP_);
});})(old_state,state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
,state,did_remount);
return this$.setState(({":rum/state": cljs.core.volatile_BANG_.call(null,next_state)}));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);

if(cljs.core.empty_QMARK_.call(null,should_update)){
} else {
goog.object.set(prototype,"shouldComponentUpdate",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (next_props,next_state){
var this$ = this;
var old_state = cljs.core.deref.call(null,rum.core.state.call(null,this$));
var new_state = cljs.core.deref.call(null,goog.object.get(next_state,":rum/state"));
var or__4131__auto__ = cljs.core.some.call(null,((function (old_state,new_state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (p1__12788_SHARP_){
return p1__12788_SHARP_.call(null,old_state,new_state);
});})(old_state,new_state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
,should_update);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return false;
}
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

if(cljs.core.empty_QMARK_.call(null,will_update)){
} else {
goog.object.set(prototype,"componentWillUpdate",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (___$1,next_state){
var this$ = this;
var new_state = goog.object.get(next_state,":rum/state");
return cljs.core._vreset_BANG_.call(null,new_state,rum.util.call_all.call(null,cljs.core._deref.call(null,new_state),will_update));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

goog.object.set(prototype,"render",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (){
var this$ = this;
var state = rum.core.state.call(null,this$);
var vec__12790 = wrapped_render.call(null,cljs.core.deref.call(null,state));
var dom = cljs.core.nth.call(null,vec__12790,(0),null);
var next_state = cljs.core.nth.call(null,vec__12790,(1),null);
cljs.core.vreset_BANG_.call(null,state,next_state);

return dom;
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);

if(cljs.core.empty_QMARK_.call(null,did_update)){
} else {
goog.object.set(prototype,"componentDidUpdate",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (___$1,___$2){
var this$ = this;
return cljs.core._vreset_BANG_.call(null,rum.core.state.call(null,this$),rum.util.call_all.call(null,cljs.core._deref.call(null,rum.core.state.call(null,this$)),did_update));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

if(cljs.core.empty_QMARK_.call(null,did_catch)){
} else {
goog.object.set(prototype,"componentDidCatch",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (error,info){
var this$ = this;
cljs.core._vreset_BANG_.call(null,rum.core.state.call(null,this$),rum.util.call_all.call(null,cljs.core._deref.call(null,rum.core.state.call(null,this$)),did_catch,error,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("rum","component-stack","rum/component-stack",2037541138),goog.object.get(info,"componentStack")], null)));

return this$.forceUpdate();
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

goog.object.set(prototype,"componentWillUnmount",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (){
var this$ = this;
if(cljs.core.empty_QMARK_.call(null,will_unmount)){
} else {
cljs.core._vreset_BANG_.call(null,rum.core.state.call(null,this$),rum.util.call_all.call(null,cljs.core._deref.call(null,rum.core.state.call(null,this$)),will_unmount));
}

return goog.object.set(this$,":rum/unmounted?",true);
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);

if(cljs.core.empty_QMARK_.call(null,child_context)){
} else {
goog.object.set(prototype,"getChildContext",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (){
var this$ = this;
var state = cljs.core.deref.call(null,rum.core.state.call(null,this$));
return cljs.core.clj__GT_js.call(null,cljs.core.transduce.call(null,cljs.core.map.call(null,((function (state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (p1__12789_SHARP_){
return p1__12789_SHARP_.call(null,state);
});})(state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
),cljs.core.merge,cljs.core.PersistentArrayMap.EMPTY,child_context));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

rum.core.extend_BANG_.call(null,prototype,class_props);

goog.object.set(ctor,"displayName",display_name);

rum.core.extend_BANG_.call(null,ctor,static_props);

return ctor;
});
rum.core.build_ctor = (function rum$core$build_ctor(render,mixins,display_name){
var class$ = rum.core.build_class.call(null,render,mixins,display_name);
var key_fn = cljs.core.first.call(null,rum.util.collect.call(null,new cljs.core.Keyword(null,"key-fn","key-fn",-636154479),mixins));
var ctor = (((!((key_fn == null))))?((function (class$,key_fn){
return (function() { 
var G__12793__delegate = function (args){
var props = ({":rum/initial-state": new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("rum","args","rum/args",1315791754),args], null), "key": cljs.core.apply.call(null,key_fn,args)});
return React.createElement(class$,props);
};
var G__12793 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__12794__i = 0, G__12794__a = new Array(arguments.length -  0);
while (G__12794__i < G__12794__a.length) {G__12794__a[G__12794__i] = arguments[G__12794__i + 0]; ++G__12794__i;}
  args = new cljs.core.IndexedSeq(G__12794__a,0,null);
} 
return G__12793__delegate.call(this,args);};
G__12793.cljs$lang$maxFixedArity = 0;
G__12793.cljs$lang$applyTo = (function (arglist__12795){
var args = cljs.core.seq(arglist__12795);
return G__12793__delegate(args);
});
G__12793.cljs$core$IFn$_invoke$arity$variadic = G__12793__delegate;
return G__12793;
})()
;})(class$,key_fn))
:((function (class$,key_fn){
return (function() { 
var G__12796__delegate = function (args){
var props = ({":rum/initial-state": new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("rum","args","rum/args",1315791754),args], null)});
return React.createElement(class$,props);
};
var G__12796 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__12797__i = 0, G__12797__a = new Array(arguments.length -  0);
while (G__12797__i < G__12797__a.length) {G__12797__a[G__12797__i] = arguments[G__12797__i + 0]; ++G__12797__i;}
  args = new cljs.core.IndexedSeq(G__12797__a,0,null);
} 
return G__12796__delegate.call(this,args);};
G__12796.cljs$lang$maxFixedArity = 0;
G__12796.cljs$lang$applyTo = (function (arglist__12798){
var args = cljs.core.seq(arglist__12798);
return G__12796__delegate(args);
});
G__12796.cljs$core$IFn$_invoke$arity$variadic = G__12796__delegate;
return G__12796;
})()
;})(class$,key_fn))
);
return cljs.core.with_meta.call(null,ctor,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("rum","class","rum/class",-2030775258),class$], null));
});
rum.core.build_defc = (function rum$core$build_defc(render_body,mixins,display_name){
if(cljs.core.empty_QMARK_.call(null,mixins)){
var class$ = (function (props){
return cljs.core.apply.call(null,render_body,(props[":rum/args"]));
});
var _ = (class$["displayName"] = display_name);
var ctor = ((function (class$,_){
return (function() { 
var G__12799__delegate = function (args){
return React.createElement(class$,({":rum/args": args}));
};
var G__12799 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__12800__i = 0, G__12800__a = new Array(arguments.length -  0);
while (G__12800__i < G__12800__a.length) {G__12800__a[G__12800__i] = arguments[G__12800__i + 0]; ++G__12800__i;}
  args = new cljs.core.IndexedSeq(G__12800__a,0,null);
} 
return G__12799__delegate.call(this,args);};
G__12799.cljs$lang$maxFixedArity = 0;
G__12799.cljs$lang$applyTo = (function (arglist__12801){
var args = cljs.core.seq(arglist__12801);
return G__12799__delegate(args);
});
G__12799.cljs$core$IFn$_invoke$arity$variadic = G__12799__delegate;
return G__12799;
})()
;})(class$,_))
;
return cljs.core.with_meta.call(null,ctor,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("rum","class","rum/class",-2030775258),class$], null));
} else {
var render = (function (state){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.apply.call(null,render_body,new cljs.core.Keyword("rum","args","rum/args",1315791754).cljs$core$IFn$_invoke$arity$1(state)),state], null);
});
return rum.core.build_ctor.call(null,render,mixins,display_name);
}
});
rum.core.build_defcs = (function rum$core$build_defcs(render_body,mixins,display_name){
var render = (function (state){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.apply.call(null,render_body,state,new cljs.core.Keyword("rum","args","rum/args",1315791754).cljs$core$IFn$_invoke$arity$1(state)),state], null);
});
return rum.core.build_ctor.call(null,render,mixins,display_name);
});
rum.core.build_defcc = (function rum$core$build_defcc(render_body,mixins,display_name){
var render = (function (state){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.apply.call(null,render_body,new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248).cljs$core$IFn$_invoke$arity$1(state),new cljs.core.Keyword("rum","args","rum/args",1315791754).cljs$core$IFn$_invoke$arity$1(state)),state], null);
});
return rum.core.build_ctor.call(null,render,mixins,display_name);
});
rum.core.schedule = (function (){var or__4131__auto__ = (function (){var and__4120__auto__ = (typeof window !== 'undefined');
if(and__4120__auto__){
var or__4131__auto__ = window.requestAnimationFrame;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var or__4131__auto____$1 = window.webkitRequestAnimationFrame;
if(cljs.core.truth_(or__4131__auto____$1)){
return or__4131__auto____$1;
} else {
var or__4131__auto____$2 = window.mozRequestAnimationFrame;
if(cljs.core.truth_(or__4131__auto____$2)){
return or__4131__auto____$2;
} else {
return window.msRequestAnimationFrame;
}
}
}
} else {
return and__4120__auto__;
}
})();
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return ((function (or__4131__auto__){
return (function (p1__12802_SHARP_){
return setTimeout(p1__12802_SHARP_,(16));
});
;})(or__4131__auto__))
}
})();
rum.core.batch = (function (){var or__4131__auto__ = (((typeof ReactNative !== 'undefined'))?ReactNative.unstable_batchedUpdates:null);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var or__4131__auto____$1 = (((typeof ReactDOM !== 'undefined'))?ReactDOM.unstable_batchedUpdates:null);
if(cljs.core.truth_(or__4131__auto____$1)){
return or__4131__auto____$1;
} else {
return ((function (or__4131__auto____$1,or__4131__auto__){
return (function (f,a){
return f.call(null,a);
});
;})(or__4131__auto____$1,or__4131__auto__))
}
}
})();
rum.core.empty_queue = cljs.core.PersistentVector.EMPTY;
rum.core.render_queue = cljs.core.volatile_BANG_.call(null,rum.core.empty_queue);
rum.core.render_all = (function rum$core$render_all(queue){
var seq__12803 = cljs.core.seq.call(null,queue);
var chunk__12805 = null;
var count__12806 = (0);
var i__12807 = (0);
while(true){
if((i__12807 < count__12806)){
var comp = cljs.core._nth.call(null,chunk__12805,i__12807);
if(cljs.core.not.call(null,goog.object.get(comp,":rum/unmounted?"))){
comp.forceUpdate();


var G__12809 = seq__12803;
var G__12810 = chunk__12805;
var G__12811 = count__12806;
var G__12812 = (i__12807 + (1));
seq__12803 = G__12809;
chunk__12805 = G__12810;
count__12806 = G__12811;
i__12807 = G__12812;
continue;
} else {
var G__12813 = seq__12803;
var G__12814 = chunk__12805;
var G__12815 = count__12806;
var G__12816 = (i__12807 + (1));
seq__12803 = G__12813;
chunk__12805 = G__12814;
count__12806 = G__12815;
i__12807 = G__12816;
continue;
}
} else {
var temp__5457__auto__ = cljs.core.seq.call(null,seq__12803);
if(temp__5457__auto__){
var seq__12803__$1 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12803__$1)){
var c__4550__auto__ = cljs.core.chunk_first.call(null,seq__12803__$1);
var G__12817 = cljs.core.chunk_rest.call(null,seq__12803__$1);
var G__12818 = c__4550__auto__;
var G__12819 = cljs.core.count.call(null,c__4550__auto__);
var G__12820 = (0);
seq__12803 = G__12817;
chunk__12805 = G__12818;
count__12806 = G__12819;
i__12807 = G__12820;
continue;
} else {
var comp = cljs.core.first.call(null,seq__12803__$1);
if(cljs.core.not.call(null,goog.object.get(comp,":rum/unmounted?"))){
comp.forceUpdate();


var G__12821 = cljs.core.next.call(null,seq__12803__$1);
var G__12822 = null;
var G__12823 = (0);
var G__12824 = (0);
seq__12803 = G__12821;
chunk__12805 = G__12822;
count__12806 = G__12823;
i__12807 = G__12824;
continue;
} else {
var G__12825 = cljs.core.next.call(null,seq__12803__$1);
var G__12826 = null;
var G__12827 = (0);
var G__12828 = (0);
seq__12803 = G__12825;
chunk__12805 = G__12826;
count__12806 = G__12827;
i__12807 = G__12828;
continue;
}
}
} else {
return null;
}
}
break;
}
});
rum.core.render = (function rum$core$render(){
var queue = cljs.core.deref.call(null,rum.core.render_queue);
cljs.core.vreset_BANG_.call(null,rum.core.render_queue,rum.core.empty_queue);

return rum.core.batch.call(null,rum.core.render_all,queue);
});
/**
 * Schedules react component to be rendered on next animation frame
 */
rum.core.request_render = (function rum$core$request_render(component){
if(cljs.core.empty_QMARK_.call(null,cljs.core.deref.call(null,rum.core.render_queue))){
rum.core.schedule.call(null,rum.core.render);
} else {
}

return cljs.core._vreset_BANG_.call(null,rum.core.render_queue,cljs.core.conj.call(null,cljs.core._deref.call(null,rum.core.render_queue),component));
});
/**
 * Add component to the DOM tree. Idempotent. Subsequent mounts will just update component
 */
rum.core.mount = (function rum$core$mount(component,node){
ReactDOM.render(component,node);

return null;
});
/**
 * Removes component from the DOM tree
 */
rum.core.unmount = (function rum$core$unmount(node){
return ReactDOM.unmountComponentAtNode(node);
});
/**
 * Hydrates server rendered DOM tree with provided component.
 */
rum.core.hydrate = (function rum$core$hydrate(component,node){
return ReactDOM.hydrate(component,node);
});
/**
 * Render `component` in a DOM `node` that might be ouside of current DOM hierarchy
 */
rum.core.portal = (function rum$core$portal(component,node){
return ReactDOM.createPortal(component,node);
});
/**
 * Adds React key to component
 */
rum.core.with_key = (function rum$core$with_key(component,key){
return React.cloneElement(component,({"key": key}),null);
});
/**
 * Adds React ref (string or callback) to component
 */
rum.core.with_ref = (function rum$core$with_ref(component,ref){
return React.cloneElement(component,({"ref": ref}),null);
});
/**
 * Given state, returns top-level DOM node. Can’t be called during render
 */
rum.core.dom_node = (function rum$core$dom_node(state){
return ReactDOM.findDOMNode(new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248).cljs$core$IFn$_invoke$arity$1(state));
});
/**
 * Given state and ref handle, returns React component
 */
rum.core.ref = (function rum$core$ref(state,key){
return ((new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248).cljs$core$IFn$_invoke$arity$1(state)["refs"])[cljs.core.name.call(null,key)]);
});
/**
 * Given state and ref handle, returns DOM node associated with ref
 */
rum.core.ref_node = (function rum$core$ref_node(state,key){
return ReactDOM.findDOMNode(rum.core.ref.call(null,state,cljs.core.name.call(null,key)));
});
/**
 * Mixin. Will avoid re-render if none of component’s arguments have changed.
 * Does equality check (=) on all arguments
 */
rum.core.static$ = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"should-update","should-update",-1292781795),(function (old_state,new_state){
return cljs.core.not_EQ_.call(null,new cljs.core.Keyword("rum","args","rum/args",1315791754).cljs$core$IFn$_invoke$arity$1(old_state),new cljs.core.Keyword("rum","args","rum/args",1315791754).cljs$core$IFn$_invoke$arity$1(new_state));
})], null);
/**
 * Mixin constructor. Adds an atom to component’s state that can be used to keep stuff
 * during component’s lifecycle. Component will be re-rendered if atom’s value changes.
 * Atom is stored under user-provided key or under `:rum/local` by default
 */
rum.core.local = (function rum$core$local(var_args){
var G__12830 = arguments.length;
switch (G__12830) {
case 1:
return rum.core.local.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return rum.core.local.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

rum.core.local.cljs$core$IFn$_invoke$arity$1 = (function (initial){
return rum.core.local.call(null,initial,new cljs.core.Keyword("rum","local","rum/local",-1497916586));
});

rum.core.local.cljs$core$IFn$_invoke$arity$2 = (function (initial,key){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"will-mount","will-mount",-434633071),(function (state){
var local_state = cljs.core.atom.call(null,initial);
var component = new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248).cljs$core$IFn$_invoke$arity$1(state);
cljs.core.add_watch.call(null,local_state,key,((function (local_state,component){
return (function (_,___$1,___$2,___$3){
return rum.core.request_render.call(null,component);
});})(local_state,component))
);

return cljs.core.assoc.call(null,state,key,local_state);
})], null);
});

rum.core.local.cljs$lang$maxFixedArity = 2;

/**
 * Mixin. Works in conjunction with `rum.core/react`
 */
rum.core.reactive = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"init","init",-1875481434),(function (state,props){
return cljs.core.assoc.call(null,state,new cljs.core.Keyword("rum.reactive","key","rum.reactive/key",-803425142),cljs.core.random_uuid.call(null));
}),new cljs.core.Keyword(null,"wrap-render","wrap-render",1782000986),(function (render_fn){
return (function (state){
var _STAR_reactions_STAR__orig_val__12832 = rum.core._STAR_reactions_STAR_;
var _STAR_reactions_STAR__temp_val__12833 = cljs.core.volatile_BANG_.call(null,cljs.core.PersistentHashSet.EMPTY);
rum.core._STAR_reactions_STAR_ = _STAR_reactions_STAR__temp_val__12833;

try{var comp = new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248).cljs$core$IFn$_invoke$arity$1(state);
var old_reactions = new cljs.core.Keyword("rum.reactive","refs","rum.reactive/refs",-814076325).cljs$core$IFn$_invoke$arity$2(state,cljs.core.PersistentHashSet.EMPTY);
var vec__12834 = render_fn.call(null,state);
var dom = cljs.core.nth.call(null,vec__12834,(0),null);
var next_state = cljs.core.nth.call(null,vec__12834,(1),null);
var new_reactions = cljs.core.deref.call(null,rum.core._STAR_reactions_STAR_);
var key = new cljs.core.Keyword("rum.reactive","key","rum.reactive/key",-803425142).cljs$core$IFn$_invoke$arity$1(state);
var seq__12837_12849 = cljs.core.seq.call(null,old_reactions);
var chunk__12838_12850 = null;
var count__12839_12851 = (0);
var i__12840_12852 = (0);
while(true){
if((i__12840_12852 < count__12839_12851)){
var ref_12853 = cljs.core._nth.call(null,chunk__12838_12850,i__12840_12852);
if(cljs.core.contains_QMARK_.call(null,new_reactions,ref_12853)){
} else {
cljs.core.remove_watch.call(null,ref_12853,key);
}


var G__12854 = seq__12837_12849;
var G__12855 = chunk__12838_12850;
var G__12856 = count__12839_12851;
var G__12857 = (i__12840_12852 + (1));
seq__12837_12849 = G__12854;
chunk__12838_12850 = G__12855;
count__12839_12851 = G__12856;
i__12840_12852 = G__12857;
continue;
} else {
var temp__5457__auto___12858 = cljs.core.seq.call(null,seq__12837_12849);
if(temp__5457__auto___12858){
var seq__12837_12859__$1 = temp__5457__auto___12858;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12837_12859__$1)){
var c__4550__auto___12860 = cljs.core.chunk_first.call(null,seq__12837_12859__$1);
var G__12861 = cljs.core.chunk_rest.call(null,seq__12837_12859__$1);
var G__12862 = c__4550__auto___12860;
var G__12863 = cljs.core.count.call(null,c__4550__auto___12860);
var G__12864 = (0);
seq__12837_12849 = G__12861;
chunk__12838_12850 = G__12862;
count__12839_12851 = G__12863;
i__12840_12852 = G__12864;
continue;
} else {
var ref_12865 = cljs.core.first.call(null,seq__12837_12859__$1);
if(cljs.core.contains_QMARK_.call(null,new_reactions,ref_12865)){
} else {
cljs.core.remove_watch.call(null,ref_12865,key);
}


var G__12866 = cljs.core.next.call(null,seq__12837_12859__$1);
var G__12867 = null;
var G__12868 = (0);
var G__12869 = (0);
seq__12837_12849 = G__12866;
chunk__12838_12850 = G__12867;
count__12839_12851 = G__12868;
i__12840_12852 = G__12869;
continue;
}
} else {
}
}
break;
}

var seq__12841_12870 = cljs.core.seq.call(null,new_reactions);
var chunk__12842_12871 = null;
var count__12843_12872 = (0);
var i__12844_12873 = (0);
while(true){
if((i__12844_12873 < count__12843_12872)){
var ref_12874 = cljs.core._nth.call(null,chunk__12842_12871,i__12844_12873);
if(cljs.core.contains_QMARK_.call(null,old_reactions,ref_12874)){
} else {
cljs.core.add_watch.call(null,ref_12874,key,((function (seq__12841_12870,chunk__12842_12871,count__12843_12872,i__12844_12873,ref_12874,comp,old_reactions,vec__12834,dom,next_state,new_reactions,key,_STAR_reactions_STAR__orig_val__12832,_STAR_reactions_STAR__temp_val__12833){
return (function (_,___$1,___$2,___$3){
return rum.core.request_render.call(null,comp);
});})(seq__12841_12870,chunk__12842_12871,count__12843_12872,i__12844_12873,ref_12874,comp,old_reactions,vec__12834,dom,next_state,new_reactions,key,_STAR_reactions_STAR__orig_val__12832,_STAR_reactions_STAR__temp_val__12833))
);
}


var G__12875 = seq__12841_12870;
var G__12876 = chunk__12842_12871;
var G__12877 = count__12843_12872;
var G__12878 = (i__12844_12873 + (1));
seq__12841_12870 = G__12875;
chunk__12842_12871 = G__12876;
count__12843_12872 = G__12877;
i__12844_12873 = G__12878;
continue;
} else {
var temp__5457__auto___12879 = cljs.core.seq.call(null,seq__12841_12870);
if(temp__5457__auto___12879){
var seq__12841_12880__$1 = temp__5457__auto___12879;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12841_12880__$1)){
var c__4550__auto___12881 = cljs.core.chunk_first.call(null,seq__12841_12880__$1);
var G__12882 = cljs.core.chunk_rest.call(null,seq__12841_12880__$1);
var G__12883 = c__4550__auto___12881;
var G__12884 = cljs.core.count.call(null,c__4550__auto___12881);
var G__12885 = (0);
seq__12841_12870 = G__12882;
chunk__12842_12871 = G__12883;
count__12843_12872 = G__12884;
i__12844_12873 = G__12885;
continue;
} else {
var ref_12886 = cljs.core.first.call(null,seq__12841_12880__$1);
if(cljs.core.contains_QMARK_.call(null,old_reactions,ref_12886)){
} else {
cljs.core.add_watch.call(null,ref_12886,key,((function (seq__12841_12870,chunk__12842_12871,count__12843_12872,i__12844_12873,ref_12886,seq__12841_12880__$1,temp__5457__auto___12879,comp,old_reactions,vec__12834,dom,next_state,new_reactions,key,_STAR_reactions_STAR__orig_val__12832,_STAR_reactions_STAR__temp_val__12833){
return (function (_,___$1,___$2,___$3){
return rum.core.request_render.call(null,comp);
});})(seq__12841_12870,chunk__12842_12871,count__12843_12872,i__12844_12873,ref_12886,seq__12841_12880__$1,temp__5457__auto___12879,comp,old_reactions,vec__12834,dom,next_state,new_reactions,key,_STAR_reactions_STAR__orig_val__12832,_STAR_reactions_STAR__temp_val__12833))
);
}


var G__12887 = cljs.core.next.call(null,seq__12841_12880__$1);
var G__12888 = null;
var G__12889 = (0);
var G__12890 = (0);
seq__12841_12870 = G__12887;
chunk__12842_12871 = G__12888;
count__12843_12872 = G__12889;
i__12844_12873 = G__12890;
continue;
}
} else {
}
}
break;
}

return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [dom,cljs.core.assoc.call(null,next_state,new cljs.core.Keyword("rum.reactive","refs","rum.reactive/refs",-814076325),new_reactions)], null);
}finally {rum.core._STAR_reactions_STAR_ = _STAR_reactions_STAR__orig_val__12832;
}});
}),new cljs.core.Keyword(null,"will-unmount","will-unmount",-808051550),(function (state){
var key_12891 = new cljs.core.Keyword("rum.reactive","key","rum.reactive/key",-803425142).cljs$core$IFn$_invoke$arity$1(state);
var seq__12845_12892 = cljs.core.seq.call(null,new cljs.core.Keyword("rum.reactive","refs","rum.reactive/refs",-814076325).cljs$core$IFn$_invoke$arity$1(state));
var chunk__12846_12893 = null;
var count__12847_12894 = (0);
var i__12848_12895 = (0);
while(true){
if((i__12848_12895 < count__12847_12894)){
var ref_12896 = cljs.core._nth.call(null,chunk__12846_12893,i__12848_12895);
cljs.core.remove_watch.call(null,ref_12896,key_12891);


var G__12897 = seq__12845_12892;
var G__12898 = chunk__12846_12893;
var G__12899 = count__12847_12894;
var G__12900 = (i__12848_12895 + (1));
seq__12845_12892 = G__12897;
chunk__12846_12893 = G__12898;
count__12847_12894 = G__12899;
i__12848_12895 = G__12900;
continue;
} else {
var temp__5457__auto___12901 = cljs.core.seq.call(null,seq__12845_12892);
if(temp__5457__auto___12901){
var seq__12845_12902__$1 = temp__5457__auto___12901;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12845_12902__$1)){
var c__4550__auto___12903 = cljs.core.chunk_first.call(null,seq__12845_12902__$1);
var G__12904 = cljs.core.chunk_rest.call(null,seq__12845_12902__$1);
var G__12905 = c__4550__auto___12903;
var G__12906 = cljs.core.count.call(null,c__4550__auto___12903);
var G__12907 = (0);
seq__12845_12892 = G__12904;
chunk__12846_12893 = G__12905;
count__12847_12894 = G__12906;
i__12848_12895 = G__12907;
continue;
} else {
var ref_12908 = cljs.core.first.call(null,seq__12845_12902__$1);
cljs.core.remove_watch.call(null,ref_12908,key_12891);


var G__12909 = cljs.core.next.call(null,seq__12845_12902__$1);
var G__12910 = null;
var G__12911 = (0);
var G__12912 = (0);
seq__12845_12892 = G__12909;
chunk__12846_12893 = G__12910;
count__12847_12894 = G__12911;
i__12848_12895 = G__12912;
continue;
}
} else {
}
}
break;
}

return cljs.core.dissoc.call(null,state,new cljs.core.Keyword("rum.reactive","refs","rum.reactive/refs",-814076325),new cljs.core.Keyword("rum.reactive","key","rum.reactive/key",-803425142));
})], null);
/**
 * Works in conjunction with `rum.core/reactive` mixin. Use this function instead of
 * `deref` inside render, and your component will subscribe to changes happening
 * to the derefed atom.
 */
rum.core.react = (function rum$core$react(ref){
if(cljs.core.truth_(rum.core._STAR_reactions_STAR_)){
} else {
throw (new Error(["Assert failed: ","rum.core/react is only supported in conjunction with rum.core/reactive","\n","*reactions*"].join('')));
}

cljs.core._vreset_BANG_.call(null,rum.core._STAR_reactions_STAR_,cljs.core.conj.call(null,cljs.core._deref.call(null,rum.core._STAR_reactions_STAR_),ref));

return cljs.core.deref.call(null,ref);
});
/**
 * Use this to create “chains” and acyclic graphs of dependent atoms.
 * `derived-atom` will:
 *  - Take N “source” refs
 *  - Set up a watch on each of them
 *  - Create “sink” atom
 *  - When any of source refs changes:
 *     - re-run function `f`, passing N dereferenced values of source refs
 *     - `reset!` result of `f` to the sink atom
 *  - return sink atom
 * 
 *  (def *a (atom 0))
 *  (def *b (atom 1))
 *  (def *x (derived-atom [*a *b] ::key
 *            (fn [a b]
 *              (str a ":" b))))
 *  (type *x) ;; => clojure.lang.Atom
 *  \@*x     ;; => 0:1
 *  (swap! *a inc)
 *  \@*x     ;; => 1:1
 *  (reset! *b 7)
 *  \@*x     ;; => 1:7
 * 
 * Arguments:
 *   refs - sequence of source refs
 *   key  - unique key to register watcher, see `clojure.core/add-watch`
 *   f    - function that must accept N arguments (same as number of source refs)
 *          and return a value to be written to the sink ref.
 *          Note: `f` will be called with already dereferenced values
 *   opts - optional. Map of:
 *     :ref           - Use this as sink ref. By default creates new atom
 *     :check-equals? - Do an equality check on each update: `(= @sink (f new-vals))`.
 *                      If result of `f` is equal to the old one, do not call `reset!`.
 *                      Defaults to `true`. Set to false if calling `=` would be expensive
 */
rum.core.derived_atom = rum.derived_atom.derived_atom;
/**
 * Given atom with deep nested value and path inside it, creates an atom-like structure
 * that can be used separately from main atom, but will sync changes both ways:
 *   
 *   (def db (atom { :users { "Ivan" { :age 30 }}}))
 *   (def ivan (rum/cursor db [:users "Ivan"]))
 *   \@ivan ;; => { :age 30 }
 *   (swap! ivan update :age inc) ;; => { :age 31 }
 *   \@db ;; => { :users { "Ivan" { :age 31 }}}
 *   (swap! db update-in [:users "Ivan" :age] inc) ;; => { :users { "Ivan" { :age 32 }}}
 *   \@ivan ;; => { :age 32 }
 *   
 *   Returned value supports deref, swap!, reset!, watches and metadata.
 *   The only supported option is `:meta`
 */
rum.core.cursor_in = (function rum$core$cursor_in(var_args){
var args__4736__auto__ = [];
var len__4730__auto___12919 = arguments.length;
var i__4731__auto___12920 = (0);
while(true){
if((i__4731__auto___12920 < len__4730__auto___12919)){
args__4736__auto__.push((arguments[i__4731__auto___12920]));

var G__12921 = (i__4731__auto___12920 + (1));
i__4731__auto___12920 = G__12921;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((2) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((2)),(0),null)):null);
return rum.core.cursor_in.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4737__auto__);
});

rum.core.cursor_in.cljs$core$IFn$_invoke$arity$variadic = (function (ref,path,p__12916){
var map__12917 = p__12916;
var map__12917__$1 = (((((!((map__12917 == null))))?(((((map__12917.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__12917.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__12917):map__12917);
var options = map__12917__$1;
if((ref instanceof rum.cursor.Cursor)){
return (new rum.cursor.Cursor(ref.ref,cljs.core.into.call(null,ref.path,path),new cljs.core.Keyword(null,"meta","meta",1499536964).cljs$core$IFn$_invoke$arity$1(options)));
} else {
return (new rum.cursor.Cursor(ref,path,new cljs.core.Keyword(null,"meta","meta",1499536964).cljs$core$IFn$_invoke$arity$1(options)));
}
});

rum.core.cursor_in.cljs$lang$maxFixedArity = (2);

/** @this {Function} */
rum.core.cursor_in.cljs$lang$applyTo = (function (seq12913){
var G__12914 = cljs.core.first.call(null,seq12913);
var seq12913__$1 = cljs.core.next.call(null,seq12913);
var G__12915 = cljs.core.first.call(null,seq12913__$1);
var seq12913__$2 = cljs.core.next.call(null,seq12913__$1);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__12914,G__12915,seq12913__$2);
});

/**
 * Same as `rum.core/cursor-in` but accepts single key instead of path vector
 */
rum.core.cursor = (function rum$core$cursor(var_args){
var args__4736__auto__ = [];
var len__4730__auto___12925 = arguments.length;
var i__4731__auto___12926 = (0);
while(true){
if((i__4731__auto___12926 < len__4730__auto___12925)){
args__4736__auto__.push((arguments[i__4731__auto___12926]));

var G__12927 = (i__4731__auto___12926 + (1));
i__4731__auto___12926 = G__12927;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((2) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((2)),(0),null)):null);
return rum.core.cursor.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4737__auto__);
});

rum.core.cursor.cljs$core$IFn$_invoke$arity$variadic = (function (ref,key,options){
return cljs.core.apply.call(null,rum.core.cursor_in,ref,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [key], null),options);
});

rum.core.cursor.cljs$lang$maxFixedArity = (2);

/** @this {Function} */
rum.core.cursor.cljs$lang$applyTo = (function (seq12922){
var G__12923 = cljs.core.first.call(null,seq12922);
var seq12922__$1 = cljs.core.next.call(null,seq12922);
var G__12924 = cljs.core.first.call(null,seq12922__$1);
var seq12922__$2 = cljs.core.next.call(null,seq12922__$1);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__12923,G__12924,seq12922__$2);
});


//# sourceMappingURL=core.js.map
