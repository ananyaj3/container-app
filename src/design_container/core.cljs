(ns ^:figwheel-hooks design-container.core
  (:require
   [goog.dom :as gdom]
   [rum.core :as rum]))

;; app setup

(defonce app-state (atom {:text "App Container Atom"}))

(defn get-app-element []
  (gdom/getElement "app"))

;; html component setup

(rum/defc header [] 
  [:head
    [:title "app-container"]])

(rum/defc body []
  [:body
  [:div {:id "viewport"}
    [:div {:id "sidebar"}
      [:header [:a {:href "#"} "Options"]]
      [:ul {:class "nav"}
        [:li [:a {:href "#"} [:i {:class "view-dashboard"}] "Dashboard"]]
        [:li [:a {:href "#"} [:i {:class "view-timeline"}] "Timeline"]]
        [:li [:a {:href "#"} [:i {:class "view-bank-and-cash"}] "Baking and Cash"]]
        [:li [:a {:href "#"} [:i {:class "view-sales"}] "Sales"]]
        [:li [:a {:href "#"} [:i {:class "view-purchases"}] "Purchases"]]
        [:li [:a {:href "#"} [:i {:class "view-contacts"}] "Contacts"]]
        [:li [:a {:href "#"} [:i {:class "view-reports"}] "Reports"]]
        [:li [:a {:href "#"} [:i {:class "view-documents"}] "Documents"]]
        [:li [:a {:href "#"} [:i {:class "view-chart-of-accounts"}] "Chart of Accounts"]]]]
    [:div {:id "content"}
      [:nav {:class "navbar-default"}
        [:div {:class "container-fluid"}
          [:ul {:class "navbar-right"}
            [:li [:a {:href "#"} "User Profile"]]
            [:li [:a {:href "#"} "Notifications"]]
            [:li [:a {:href "#"} "Help"]]]]]
      [:div {:class "content-class"}
        [:h1 "Basic Container App"]
        [:p "Keep Content Here"]]]]])  



(rum/defc content []
[:div {}
  (header)
  (body)])


;; All code below is done for mounting content

(defn mount [el]
  (rum/mount (content) el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

(mount-app-element)

(defn ^:after-load on-reload []
  (mount-app-element)
)
